#include "looker_master.h"
#include "looker_stubs.h"
#include "wifi.h"

#define LOOKER_DOMAIN "PIEC"


#include <EEPROM.h>

#include "TimerOne.h"
#include "SevenSegmentTM1637.h"

//clk d4, di d3
#define sbi(sfr,bit)  sfr |= _BV(bit)
#define cbi(sfr,bit)  sfr &= ~(_BV(bit)) 
#define MELT 1.2
#define NMAX 80
#define Ta 40
#define Tb 400
#define REF 2.434
#define NSAMP 120
#define Ku 155.9
#define vnz 32.7

//SoftwareSerial mySerial(8, 7); // RX, TX

volatile long NN=0;
volatile uint16_t u=0,pidon=0,phase=0, utmp;
volatile char vb=0;

volatile float setpoint, temperature, err, intg=0.0, perr , si=0.1 ,sp=2.0, sd=0.0, sstop=50, sinc=0.5, sc,tlm35;
char buf[NMAX];
static float oldv,dv;

const byte PIN_CLK = 4;   // define CLK pin (any digital pin)
const byte PIN_DIO = 3;   // define DIO pin (any digital pin)
SevenSegmentTM1637    display(PIN_CLK, PIN_DIO);




float pw(float x, uint8_t n)
{
uint8_t k;
float temp=x;
if (n==0) return 1.0f;
if (n==1) return x;
for (k=0; k<n-1; k++) temp*=x;
   return temp;


}


float ths(float t)
{
uint8_t n;
float temp=0;
float tab1[]={-8.087801117E+01,1.621573104E+02,-8.536869453E+00,4.719686976E-01,-1.441693666E-02,2.081618890E-04};
float tab2[]={1.291507177E+01,1.466298863E+02,-1.534713402E+01,3.145945973E+00,-4.163257839E-01,3.187963771E-02,-1.291637500E-03,
                                2.183475087E-05,-1.447379511E-07,8.211272125E-09};
float tab3[]={0.00000000E+00,1.84949460E+02,-8.00504062E+01,1.02237430E+02,
                -1.52248592E+02,1.88821343E+02,-1.59085941E+02,8.23027880E+01,
                -2.34181944E+01,2.79786260E+00};
if (t>11.95) 
{
 for  (n=0; n<6; n++) temp+=pw(t,n)*tab1[n];
 return temp;
}
if (t<1.874) 
{
 for  (n=0; n<10; n++) temp+=pw(t,n)*tab3[n];
 return temp;
} else
{
  for  (n=0; n<10; n++) temp+=pw(t,n)*tab2[n];
  return temp;

}





}



void setv()
{
char j=0,k=0;
char smallbuf[25];
if (buf[0]=='S')
for (int i=1; ((buf[i]!=0)&&(i<NMAX)); i++)
{
  if (buf[i]!=',') smallbuf[j++]=buf[i]; else
  { 
    smallbuf[++j]=0;
    switch (k++){
    case 0 :      sp=strtod(smallbuf,NULL); break;
    case 1 :      si=strtod(smallbuf,NULL); break;
    case 2 :      sd=strtod(smallbuf,NULL); break;
    case 3 :      sc=strtod(smallbuf,NULL); break;
    
    }
  j=0;
  }
}
if (buf[0]=='T')
for (int i=1; ((buf[i]!=0)&&(i<NMAX)); i++)
{
  if (buf[i]!=',') smallbuf[j++]=buf[i]; else
  { 
    smallbuf[++j]=0;
    switch (k++){
    case 0 :      sstop=strtod(smallbuf,NULL); pidon=1; setpoint=temperature; break;
    case 1 :      sinc=strtod(smallbuf,NULL); break;    
    }
  j=0;
  }
}
if (buf[0]=='P')
for (int i=1; ((buf[i]!=0)&&(i<NMAX)); i++)
{
  if (buf[i]!=',') smallbuf[j++]=buf[i]; else
  { 
    smallbuf[++j]=0;
    switch (k++){
    case 0 :      phase=atoi(smallbuf);  break;
    }
  j=0;
  }
}


if (buf[0]=='V') vb=1;
if (buf[0]=='v') vb=0;



}



void update()
{
  Timer1.pwm(9, phase); 
}


float readV()
{
 float tmp=0,tmp2; 
 for (int ii=0; ii<NSAMP; ii++) tmp+=((float)analogRead(A1));
 tmp/=(float)NSAMP;
 tmp2=(1000.0*REF*tmp/1024-32.7)/Ku; 
return tmp2; 
}



void  write_str(int adr,char *bf)
{
   int i=0;
   do{
    
   EEPROM.write(adr+i,bf[i]);
   i++;
   } while (bf[i]!=0);
   EEPROM.write(adr+i,0);
   EEPROM.write(adr+i+1,0);
}

void  read_str(int adr,char *bf)
{
  int i=0;
  do{
  bf[i]=EEPROM.read(adr+i);
  i++;
  } while (bf[i-1]!=0);
  bf[i]=0;
}

 
void setup()
{
  pinMode(9, OUTPUT);
//  pinMode(4, OUTPUT);
//  pinMode(3, OUTPUT);
  display.begin();    
  display.setBacklight(50);
  display.print("JG");
  //pinMode(A0, INPUT);
  analogReference(EXTERNAL);
  Timer1.initialize(1000000);         // initialize timer1, and set a 1/2 second period
  Timer1.attachInterrupt(update);  // attaches callback() as a timer overflow interrupt
  setpoint=ths(readV()+0.185);
  Serial.begin(115200);
  Serial.println("PID controller v 2018, Jerzy Goraus\n");  
  if (EEPROM.read(10)=='T')
  {
  read_str(10,buf);
  Serial.println("From EEPROM READ: ");
  Serial.println(buf);
  setv();
  }

if (EEPROM.read(110)=='S')
  {
  read_str(110,buf);
  Serial.println("From EEPROM READ: ");
  Serial.println(buf);
  setv();
  
}
setpoint=ths(readV()+0.185);  
oldv=readV();

looker_stubs_init(NULL);
looker_wifi_connect(LOOKER_SSID, LOOKER_PASS, LOOKER_DOMAIN);

looker_reg("Ts", &temperature, sizeof(temperature), LOOKER_TYPE_FLOAT_2, LOOKER_LABEL_VIEW, NULL);
looker_reg("T0", &sstop, sizeof(sstop), LOOKER_TYPE_FLOAT_0, LOOKER_LABEL_EDIT, NULL);
//Serial.print("--");
looker_reg("St", &sinc, sizeof(sinc), LOOKER_TYPE_FLOAT_0, LOOKER_LABEL_EDIT, NULL);

looker_reg("Setpoint", &setpoint, sizeof(setpoint), LOOKER_TYPE_FLOAT_0, LOOKER_LABEL_EDIT, NULL);



looker_reg("P", &sp, sizeof(sp), LOOKER_TYPE_FLOAT_0, LOOKER_LABEL_EDIT, NULL);
looker_reg("I", &si, sizeof(si), LOOKER_TYPE_FLOAT_0, LOOKER_LABEL_EDIT, NULL);
looker_reg("D", &sp, sizeof(sp), LOOKER_TYPE_FLOAT_0, LOOKER_LABEL_EDIT, NULL);
looker_reg("ON", &pidon, sizeof(pidon), LOOKER_TYPE_UINT, LOOKER_LABEL_EDIT, NULL);


}



void printall()
{
 Serial.print("setpoint: ");
 Serial.print(" ");
 Serial.print(setpoint,6);
   
Serial.print(", temperature: ");
 Serial.print(" ");
 Serial.print(temperature,6);

Serial.print(", final temperature: ");
 Serial.print(" ");
 Serial.print(sstop,6);
 

Serial.print(", rise: ");
 Serial.print(" ");
 Serial.println(sinc,6);

Serial.print("lm35:  ");
 Serial.print(tlm35);
Serial.print(", phase:  ");
 Serial.println(phase);


Serial.print("PIDC constants:  ");
 Serial.print(" ");  Serial.print(sp,6); Serial.print(" ");  Serial.print(si,6); 
 Serial.print(" ");  Serial.print(sd,6); Serial.print(" ");  Serial.println(sc,6);


  
}

void eev()
{

  if (buf[0]=='T') { 
for (int i=0; i<60; i++) EEPROM.write(10+i,0);
    write_str(10,buf);
Serial.println("T: ");
Serial.println(buf);

}
if (buf[0]=='S') {
for (int i=0; i<60; i++) EEPROM.write(110+i,0);

  write_str(110,buf);
Serial.println("S: ");
Serial.println(buf);
}

}



void loop() {
  // put your main code here, to run repeatedly:


int16_t dtmp;

    float v;
   v=readV();
   dv=0.03*abs(v-oldv)+0.97*(v-oldv);
   oldv=v;
   
  // v=(float) analogRead(A1);
   // Serial.println(dv);
    temperature=0.03*ths(v+0.185)+0.97*temperature; //0.125 U for 22C
    if (temperature>1350) { phase=0; Serial.println("protection T>1350, phase=0"); pidon=0; }
    if (dv>0.2) {phase=0; Serial.println("protection dv>0.2, phase=0, check thermocouple!"); pidon=0; display.print("E"); }
 //   Serial.println(temperature);
int16_t itemp=(int16_t) temperature;
display.clear();
display.print(itemp);
looker_update();
delay(10);

if (pidon>0) 
{
 
  err=setpoint-temperature;
    intg+=si*err;
    
    if (intg>1000) intg=1000.0;
    if (intg<-1000) intg=-1000.0;
    dtmp=(sc+sp*err+intg+sd*(err-perr));
 /*   if ((NN++%100)==0){
    Serial.println("err:");
    Serial.println(err);
    Serial.println("perr:");
    Serial.println(perr);
    Serial.println("intg:");
    Serial.println(intg);
    Serial.println("phase:");
    Serial.println(phase);
  }
  */  
    if (dtmp<0) dtmp=0;
    if (dtmp>1000) dtmp=1000; 
    phase=(uint16_t) dtmp;
    perr=err;
    if (sstop>100)
    {
    setpoint+=sinc;
    if (((setpoint>sstop)&&(sinc>0))||((setpoint<sstop)&&(sinc<0)))
    {
     sstop=0;
      Serial.println("TARGET REACHED\n\r");
      }
  } 
}
if (vb==1)
  {
/*    Serial.print(temperature);
    Serial.print(" ");
    Serial.print(setpoint);
    Serial.print(" ");
    Serial.print(phase);
    Serial.println();*/
    printall();
  }


if (Serial.available())
{
  buf[0]=0;
  int i=0;
  for (int ii=0; ii<NMAX-1; ii++) buf[ii]=0;  
  while (Serial.available()) {
      delay(1);  //small delay to allow input buffer to fill
    char c = Serial.read();  //gets one byte from serial buffer
    if (c == '\n') {
      buf[i]=0;
          break; //breaks out of capture loop to print readstring
    }
    if (i>NMAX-1) break;
    buf[i++]=c; //makes the string readString
    }
  eev();  
  setv(); 
  printall();   
}
if (sstop>1350) sstop=0;
if (setpoint>1350) setpoint=0;
if (sinc>0.01) sinc=0.01;

}
